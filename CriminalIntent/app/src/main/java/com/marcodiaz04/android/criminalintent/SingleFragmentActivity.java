package com.marcodiaz04.android.criminalintent;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by M8S1 on 10/5/2017.
 */

public abstract class SingleFragmentActivity extends AppCompatActivity {
    //this activity will manage a CrimeFragment fragment inside

    protected abstract Fragment createFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);  //Set the layout where the container is
        //You always set the Fragment manager like this (ON THE SUPPORT LIBRARY
        FragmentManager fm = getSupportFragmentManager();
        //this was a weird thing that made me lose time, the fragment_container was missing lol
        //The fix was going into the container and add the id to the FrameLayour ezpz
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);

        //Check if the fragment is null, because if it is then you will ADD(.add) a fragment
        if(fragment == null){
            fragment = createFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }
        //This is the bread a butter of a fragment, remember clearly
        //YOURFRAGMENTMANAGER.beginTransaction().XXX.(R.id.YOURIDINYOURCONTAINER, YOURFRAGMENT).commit();
        // notice that the XXX could be {add,remove,attach,detach or replace}. TODO check the container

    }
}
