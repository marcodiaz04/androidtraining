package com.marcodiaz04.android.criminalintent;

import android.support.v4.app.Fragment;




public class CrimeActivity extends SingleFragmentActivity {


    @Override
    protected Fragment createFragment() {
        return new CrimeFragment();
    }
}
