package com.marcodiaz04.android.criminalintent;

import android.support.v4.app.Fragment;

/**
 * Created by M8S1 on 10/5/2017.
 */

public class CrimeListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new CrimeListFragment();
    }
}
